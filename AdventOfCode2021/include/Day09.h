#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <span>
#include <algorithm>
#include <utility>

#include "Utilities.h"

namespace day09 {
	typedef std::pair<int, int> Point;

	class Grid {
	public:
		Grid(std::span<std::vector<int>> data) {
			for (const auto& row : data) {
				grid.push_back(row);

				std::vector basinrow(row.size(), -1);
				basin.push_back(basinrow);
			}
		}

		int scoreLow() {
			markLowPoints();

			int sum = 0;
			for (auto& pt : lowPoints)
				sum += 1 + getGrid(pt);

			return sum;
		}

		void markLowPoints() {
			for (int y = 0; y < grid.size(); y++) {
				for (int x = 0; x < grid[y].size(); x++) {

					int height = getGrid(x, y);

					bool isLowPoint = true;
					for (auto& neighbor : getNeighbors({ x, y })) {

						if (getGrid(neighbor) <= height) {
							isLowPoint = false;
							break;
						}
					}

					if (isLowPoint) {
						lowPoints.emplace_back(x, y);
						lowPointCount++;
						setBasin(x, y, lowPointCount);
					}
				}
			}

			basinSize.resize(size_t(lowPointCount) + 1);
			for (auto& b : basinSize)
				b = 0;
		}

		std::vector<Point> getNeighbors(Point p) {
			auto [x, y] = p;
			int xmin = x - 1;
			int xmax = x + 1;
			int ymin = y - 1;
			int ymax = y + 1;

			std::vector<Point> pts;

			if (ymin >= 0) pts.emplace_back(x, ymin);
			if (xmin >= 0) pts.emplace_back(xmin, y);
			if (xmax < grid[0].size()) pts.emplace_back(xmax, y);
			if (ymax < grid.size()) pts.emplace_back(x, ymax);

			return pts;
		}

		void flood(Point pt, int id) {
			setBasin(pt, id);
			basinSize[id] += 1;

			for (auto& n : getNeighbors(pt))
				if (getGrid(n) != 9 && getBasin(n) == -1)
					flood(n, id);
		}

		void markBasins() {
			if (lowPoints.size() == 0)
				markLowPoints();

			for (const auto& pt : lowPoints)
				flood(pt, getBasin(pt));
		}

		int threeLargestProduct() {
			std::sort(basinSize.begin(), basinSize.end());

			auto i = basinSize.size() - 1;
			return basinSize[i] * basinSize[i - 1] * basinSize[i - 2];
		}

		int getGrid(int x, int y) const {
			return grid[y][x];
		}

		int getGrid(Point p) const {
			return grid[p.second][p.first];
		}

		int getBasin(int x, int y) const {
			return basin[y][x];
		}

		int getBasin(Point p) const {
			return basin[p.second][p.first];
		}

		void setBasin(int x, int y, int value) {
			basin[y][x] = value;
		}

		void setBasin(Point p, int value) {
			basin[p.second][p.first] = value;
		}

		void print() const {
			for (auto& g : grid)
				fprint("{}\n", g);
			for (auto& b : basin)
				fprint("{}\n", b);
			fprint("Basin sizes: {}\n", basinSize);
		}

	private:
		std::vector<std::vector<int>> grid;
		std::vector<std::vector<int>> basin;
		std::vector<int> basinSize;
		std::vector<Point> lowPoints;
		int lowPointCount{ 0 };
	};

	void part01(Grid& grid) {
		fprintn("\n[Day 9]\n  Part 1");
		fprintn("\tLow Score: {}", grid.scoreLow());
	}

	void part02(Grid& grid) {
		fprintn("  Part 2");
		grid.markBasins();
		fprintn("\tProduct of three largest basins: {}", grid.threeLargestProduct());
	}

	void run() {
		std::vector<std::vector<int>> data;

		std::ifstream input{ "./data/day09/input.txt" };
		if (input.is_open())
		{
			std::string line;
			while (std::getline(input, line))
			{
				std::vector<int> row;
				for (char c : line)
					row.push_back((int)c - 48);
				data.push_back(row);
			}
			input.close();
		}

		Grid grid{ data };
		part01(grid);
		part02(grid);
	}

} // namespace day09