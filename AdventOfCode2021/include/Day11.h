#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <string_view>
#include <vector>
#include <span>
#include <algorithm>

#include "Utilities.h"

namespace day11 {
	class Grid {
	public:
		Grid() {}

		void incrementAll() {
			for (auto y = 0; y < grid.size(); y++)
				for (auto x = 0; x < grid.size(); x++)
					increment(x, y);
		}

		void resetFlashMask() {
			for (auto y = 0; y < flashMask.size(); y++)
				for (auto x = 0; x < flashMask.size(); x++)
					setFlash(x, y, 0);
		}

		bool syncReached() {
			for (auto y = 0; y < flashMask.size(); y++)
				for (auto x = 0; x < flashMask.size(); x++)
					if (getFlash(x, y) == 0)
						return false;
			return true;
		}

		void propagateFlashToNeighbors(int x, int y) {
			// 3x3 window centered around x,y
			// we only increase if the cell has not flashed previously this timestep
			auto xmin = limit(x - 1);
			auto xmax = limit(x + 1);
			auto ymin = limit(y - 1);
			auto ymax = limit(y + 1);

			for (auto y = ymin; y <= ymax; y++)
				for (auto x = xmin; x <= xmax; x++)
					if (getFlash(x, y) == 0)
						increment(x, y);
		}

		void timestep(int step) {
			resetFlashMask();
			incrementAll();

			// continuously propgate flashes til no more squid have the energy to flash in this timestep
			while (1) {
				bool flashing = false;

				for (auto y = 0; y < grid.size(); y++) {
					for (auto x = 0; x < grid.size(); x++) {

						// check if this cell is ready to flash
						if (get(x, y) >= 10) {
							set(x, y, 0);
							setFlash(x, y, 1);

							propagateFlashToNeighbors(x, y);

							totalFlashes++;
							flashing = true;
						}
					}
				}

				if (!flashing) break;
			}
		}

		void simulate(int steps = 100) {
			for (auto i = 1; i < steps + 1; i++)
				timestep(i);
		}

		int simulateTilSync() {
			for (auto i = 1; i < 9999999; i++) {
				timestep(i);

				if (syncReached())
					return i;
			}
			return 0;
		}

		void pushLine(std::string_view line) {
			std::vector<int> row;
			std::vector<int> maskRow;

			for (char c : line) {
				row.push_back((int)c - 48);
				maskRow.push_back(0);
			}

			grid.push_back(row);
			flashMask.push_back(maskRow);
		}

		void print() const {
			for (auto& line : grid)
				fprint("{}\n", line);
		}

		int get(int x, int y) const {
			return grid[y][x];
		}

		void set(int x, int y, int val) {
			grid[y][x] = val;
		}

		int getFlash(int x, int y) const {
			return flashMask[y][x];
		}

		void setFlash(int x, int y, int val) {
			flashMask[y][x] = val;
		}

		long long int getTotalFlashes() const {
			return totalFlashes;
		}

		void increment(int x, int y) {
			grid[y][x]++;
		}

		int limit(int v) {
			// grid is symmetrical
			return std::clamp(v, 0, static_cast<int>(grid.size() - 1));
		}

	private:
		long long int totalFlashes{ 0 };
		std::vector<std::vector<int>> grid;
		std::vector<std::vector<int>> flashMask;
	};

	// we want to copy grid
	void part01(Grid grid) {
		fprintn("\n[Day 11]\n  Part 1");

		grid.simulate(100);

		fprintn("\tTotal Flashes: {}", grid.getTotalFlashes());
	}

	// we want to copy grid
	void part02(Grid grid) {
		fprintn("  Part 2");

		int syncStep = grid.simulateTilSync();

		fprintn("\tSync Step: {}", syncStep);
	}

	void run() {
		Grid grid;

		std::ifstream input{ "./data/day11/input.txt" };
		if (input.is_open())
		{
			std::string line;
			while (std::getline(input, line))
				grid.pushLine(line);
			input.close();
		}

		part01(grid);
		part02(grid);
	}

} // namespace day11