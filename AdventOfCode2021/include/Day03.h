#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <span>
#include <bitset>

#include "Utilities.h"

namespace day03 {
	const std::size_t width{ 12 };

	void part01(std::span<std::vector<int>> data) {
		std::bitset<width> gamma;

		for (auto i = 0; i < width; i++) {
			auto zero = 0;
			auto one = 0;

			for (const auto& line : data) {
				if (line[i] == 0)
					zero++;
				else
					one++;
			}

			gamma.set(width - i - 1, one > zero ? 1 : 0);
		}

		auto gammaSolve = gamma.to_ulong();
		gamma.flip();
		auto epsilonSolve = gamma.to_ulong();

		std::cout << "\n[Day 3]\n  Part 1\n";
		std::cout << "    Gamma:   " << gammaSolve << std::endl;
		std::cout << "    Epsilon: " << epsilonSolve << std::endl;
		std::cout << "    Power consumption:   " << gammaSolve * epsilonSolve << std::endl;
	}

	std::bitset<width> rating(std::span<std::vector<int>> data, bool keepHigher) {
		int validCount = static_cast<int>(data.size());

		std::vector<int> valid;
		for (auto i = 0; i < validCount; i++) {
			valid.push_back(true);
		}

		for (auto i = 0; i < width; i++) {
			if (validCount == 1)
				break;

			auto zero = 0;
			auto one = 0;

			// summation pass //
			for (auto j = 0; j < data.size(); j++) {
				if (valid[j] == 0)
					continue;

				if (data[j][i] == 0)
					zero++;
				else
					one++;
			}

			// elimination pass //
			for (auto j = 0; j < data.size(); j++) {
				if (validCount == 1)
					break;
				if (valid[j] == 0)
					continue;

				const auto& line = data[j];

				if (keepHigher) {
					if (line[i] == 0 && one >= zero) {
						valid[j] = false;
						validCount--;
					}
					else if (line[i] == 1 && one < zero) {
						valid[j] = false;
						validCount--;
					}
				}
				else {
					if (zero > one && line[i] == 0) {
						valid[j] = false;
						validCount--;
					}
					else if (one >= zero && line[i] == 1) {
						valid[j] = false;
						validCount--;
					}
				}
			}
		}

		std::bitset<width> result;
		for (auto j = 0; j < data.size(); j++) {
			if (valid[j] == 1) {
				const auto& line = data[j];

				for (auto k = 0; k < line.size(); k++)
					result.set(width - 1 - k, line[k]);

				break;
			}
		}

		return result;
	}

	void part02(std::span<std::vector<int>> data) {

		auto oxygen = rating(data, true);
		auto c02 = rating(data, false);

		std::cout << "  Part 2\n";
		std::cout << "    Oxygen:   " << oxygen.to_ulong() << std::endl;
		std::cout << "    C02:      " << c02.to_ulong() << std::endl;
		std::cout << "    Life support rating: " << oxygen.to_ulong() * c02.to_ulong() << std::endl;

	}

	void run() {
		std::vector<std::vector<int>> data;

		std::ifstream input("./data/day03/input.txt");
		if (input.is_open())
		{
			std::string line;
			while (std::getline(input, line))
			{
				std::vector<int> binary;
				for (char const& c : line) {
					binary.push_back((int)c - 48);
				}

				data.emplace_back(binary);
			}
			input.close();
		}

		part01(data);
		part02(data);
	}

} // namespace day03