#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <span>
#include <cstdint>
#include <limits>
//#include <deque>

#include "Utilities.h"

namespace day15 {
	typedef std::uint8_t ui8;
	typedef std::int32_t i32;

	struct Pos {
		Pos(size_t _x, size_t _y) : x(i32(_x)), y(i32(_y)) {}
		Pos(i32 _x, i32 _y) : x(_x), y(_y) {}

		friend bool operator!= (const Pos& p1, const Pos& p2)
		{
			return (p1.x != p2.x || p1.y != p2.y);
		}

		friend bool operator== (const Pos& p1, const Pos& p2)
		{
			return (p1.x == p2.x && p1.y == p2.y);
		}

		i32 x{ 0 };
		i32 y{ 0 };
	};

	class Grid {
	public:
		Grid(const std::span<std::vector<int>> data) {
			mDimX = data[0].size();
			mDimY = data.size();

			for (const auto& row : data) {
				for (const auto x : row) {
					mRisk.push_back(x);
					mVisited.push_back(false);
					mCost.push_back(std::numeric_limits<i32>::max());
				}
			}
		}

		Grid(const std::span<std::vector<int>> data, int tiles) {
			i32 tileDimX = i32(data[0].size());
			i32 tileDimY = i32(data.size());

			mDimX = tileDimX * tiles;
			mDimY = tileDimY * tiles;

			auto xysize = mDimX * mDimY;
			mRisk.resize(xysize);
			mVisited.resize(xysize);
			mCost.resize(xysize);

			// iterate all tiles
			for (i32 k = 0; k < tiles; k++) {
				for (i32 j = 0; j < tiles; j++) {
					// single tile
					for (i32 y = 0; y < tileDimY; y++) {
						for (i32 x = 0; x < tileDimX; x++) {

							i32 offx = x + j * tileDimX;
							i32 offy = y + k * tileDimY;
							i32 offsetVal = j + k;

							// wrap to 1, not zero, so we add an additional (val / 10) term
							// note that int division rounds towards zero
							auto val = (i32(data[y][x]) + offsetVal);
							val = (val % 10) + (val / 10);

							setRisk(offx, offy, val);
							setVisited(offx, offy, false);
							setCost(offx, offy, std::numeric_limits<i32>::max());
						}
					}
				}
			}
		}

		std::vector<Pos> getUnvisitedNeighbors(Pos p) const {
			std::vector<Pos> nbrs;

			i32 xmin = i32(p.x) - 1;
			i32 xmax = i32(p.x) + 1;
			i32 ymin = i32(p.y) - 1;
			i32 ymax = i32(p.y) + 1;

			if (xmin > 0 && !getVisited(xmin, p.y))
				nbrs.emplace_back(xmin, p.y);
			if (xmax < mDimX && !getVisited(xmax, p.y))
				nbrs.emplace_back(xmax, p.y);
			if (ymin > 0 && !getVisited(p.x, ymin))
				nbrs.emplace_back(p.x, ymin);
			if (ymax < mDimY && !getVisited(p.x, ymax))
				nbrs.emplace_back(p.x, ymax);

			return nbrs;
		}

		void sortNext() {
			std::sort(mNext.begin(), mNext.end(), [&](const Pos& a, const Pos& b) {
				return getCost(a) > getCost(b);
				});
		}

		void dijkstra() {
			Pos currentPos{ 0,0 };
			Pos endPos{ mDimX - 1, mDimY - 1 };

			// start top left. cost is zero regaurdless of risk
			setCost(currentPos, 0);
			setVisited(currentPos, true);

			for (const auto& nbr : getUnvisitedNeighbors(currentPos))
				mNext.push_back(nbr);

			for (const auto& nbr : mNext)
				setCost(nbr, getCost(currentPos) + getRisk(nbr));

			while (currentPos != endPos) {

				sortNext();
				currentPos = mNext.back();
				mNext.pop_back();

				setVisited(currentPos);

				for (const auto& nbr : getUnvisitedNeighbors(currentPos)) {
					// cost to the neighbor through the current grid cell
					auto costTo = getCost(currentPos) + getRisk(nbr);
					if (costTo < getCost(nbr))
						setCost(nbr, costTo);

					// queue up this neighbor if new
					if (std::find(mNext.begin(), mNext.end(), nbr) == mNext.end())
						mNext.push_back(nbr);
				}
			}
		}

		void setRisk(i32 x, i32 y, ui8 value) {
			mRisk[mDimX * y + x] = value;
		}

		ui8 getRisk(Pos p) const {
			return mRisk[mDimX * p.y + p.x];
		}

		void setVisited(Pos p, ui8 value = true) {
			mVisited[mDimX * p.y + p.x] = value;
		}

		void setVisited(i32 x, i32 y, ui8 value = true) {
			mVisited[mDimX * y + x] = value;
		}

		ui8 getVisited(Pos p) const {
			return mVisited[mDimX * p.y + p.x];
		}

		ui8 getVisited(i32 x, i32 y) const {
			return mVisited[mDimX * y + x];
		}

		void setCost(Pos p, i32 value) {
			mCost[mDimX * p.y + p.x] = value;
		}

		void setCost(i32 x, i32 y, i32 value) {
			mCost[mDimX * y + x] = value;
		}

		i32 getCost(Pos p) const {
			return mCost[mDimX * p.y + p.x];
		}

		i32 getCost(i32 x, i32 y) const {
			return mCost[mDimX * y + x];
		}

		i32 getCostBottomRight() {
			return getCost(Pos(mDimX - 1, mDimY - 1));
		}

		void print() const {
			fprintn("\nRisk:");
			for (auto y = 0; y < mDimY; y++) {
				for (auto x = 0; x < mDimX; x++)
					fprint("{:1}", getRisk(Pos(x, y)));
				fprint("\n");
			}

			fprintn("\nVisited:");
			for (auto y = 0; y < mDimY; y++) {
				for (auto x = 0; x < mDimX; x++) {
					if (getVisited(x, y))
						fprint(" +");
					else
						fprint(" .");
				}
				fprint("\n");
			}

			fprintn("\nCost:");
			for (auto y = 0; y < mDimY; y++) {
				for (auto x = 0; x < mDimX; x++) {
					auto cost = getCost(x, y);
					if (cost == std::numeric_limits<i32>::max())
						fprint("   .");
					else
						fprint("{:4}", cost);
				}
				fprint("\n");
			}
		}

	private:
		size_t mDimX;
		size_t mDimY;
		std::vector<ui8> mRisk;
		std::vector<ui8> mVisited;
		std::vector<i32> mCost;
		std::vector<Pos> mNext;
	};

	void part01(Grid& data) {
		fprint("\n[Day 15]\n  Part 1\n");

		data.dijkstra();
		//data.print();
		fprintn("\tMinimum total risk: {}", data.getCostBottomRight());
	}

	void part02(Grid& data) {
		fprint("  Part 2\n");

		data.dijkstra();
		//data.print();
		fprintn("\tMinimum total risk, 5 tiles: {}", data.getCostBottomRight());
	}

	void run() {
		std::vector<std::vector<int>> data;

		std::ifstream input{ "./data/day15/input.txt" };
		if (input.is_open())
		{
			std::string line;
			while (std::getline(input, line))
				data.push_back(splitDigits(line));
			input.close();
		}

		Grid grid{ data };
		Grid gridTiled{ data, 5 };

		part01(grid);
		part02(gridTiled);
	}

} // namespace day15#pragma once
