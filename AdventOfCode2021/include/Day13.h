#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <span>
#include <utility>

#include "Utilities.h"

namespace day13 {

	class Grid {
	public:
		Grid(const std::span<std::pair<int, int>> dots, const std::vector<std::pair<int, int>>& foldSteps, const std::pair<int, int>& maxSize) :
			folds{ foldSteps },
			gridSize{ maxSize } {

			grid.resize(maxSize.second);

			for (auto y = 0; y < maxSize.second; y++)
				grid[y] = std::vector<int>(maxSize.first, 0);

			for (auto [x, y] : dots)
				grid[y][x] = 1;
		}

		void foldOnce() {
			auto [xfold, yfold] = folds[currentFold];
			currentFold++;

			if (xfold != 0) {
				for (auto y = 0; y < gridSize.second; y++) {
					for (auto x = xfold + 1; x < gridSize.first; x++) {
						if (grid[y][x] == 1) {
							grid[y][x] = 0;
							auto newx = xfold + (xfold - x);
							grid[y][newx] = 1;
						}
					}
				}
				gridSize.first = xfold; // shrink compute area of grid
			}
			else {
				for (auto y = yfold + 1; y < gridSize.second; y++) {
					for (auto x = 0; x < gridSize.first; x++) {
						if (grid[y][x] == 1) {
							grid[y][x] = 0;
							auto newy = yfold + (yfold - y);
							grid[newy][x] = 1;
						}
					}
				}
				gridSize.second = yfold; // shrink compute area of grid
			}
		}

		void foldAll() {
			while (currentFold < folds.size())
				foldOnce();
		}

		int dotCount() {
			auto sum = 0;

			for (auto& row : grid)
				for (auto x : row)
					sum += x;

			return sum;
		}

		void print() {
			for (auto y = 0; y < gridSize.second; y++) {
				for (auto x = 0; x < gridSize.first; x++) {
					if (grid[y][x] == 1)
						fprint("#");
					else
						fprint(" ");
				}
				fprint("\n");
			}
		}

	private:
		std::vector<std::vector<int>> grid;
		std::pair<int, int> gridSize;
		std::vector<std::pair<int, int>> folds;
		std::size_t currentFold{ 0 };
	};

	void part01(Grid& grid, std::vector<std::pair<int, int>> folds) {
		fprintn("\n[Day 13]\n  Part 1");

		grid.foldOnce();
		fprintn("\tDots after one fold: {}", grid.dotCount());
	}

	void part02(Grid& grid, std::vector<std::pair<int, int>> folds) {
		fprintn("  Part 2");

		grid.foldAll();
		fprintn("\tThermal Code:");
		grid.print();
	}

	void run() {
		std::vector<std::pair<int, int>> dots;
		std::vector<std::pair<int, int>> folds;
		std::pair<int, int> maxSize{ 1,1 };

		std::ifstream input{ "./data/day13/input.txt" };
		if (input.is_open())
		{
			std::string line;
			while (std::getline(input, line))
			{
				if (line.starts_with("fold")) {
					auto [a, b] = splitPair(line, "=");
					if (a.ends_with("x"))
						folds.emplace_back(std::stoi(b), 0);
					else
						folds.emplace_back(0, std::stoi(b));
				}
				else if (line.size() > 2) {
					auto [x, y] = splitPairInt(line, ",");

					if (x + 1 > maxSize.first)
						maxSize.first = x + 1;
					if (y + 1 > maxSize.second)
						maxSize.second = y + 1;

					dots.emplace_back(x, y);
				}
			}
			input.close();
		}

		auto grid = Grid{ dots, folds, maxSize };

		part01(grid, folds);
		part02(grid, folds);
	}

} // namespace day13