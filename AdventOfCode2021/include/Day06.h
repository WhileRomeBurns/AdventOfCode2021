#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <span>
#include <cstdint>
#include <cmath>
#include <cstdint>

#include "Utilities.h"

namespace day06 {
	typedef std::uint8_t ui8;
	typedef std::uint64_t ui64;

	// we actually want a copy here, we'll be growing the vec
	void part01(std::vector<int> fish) {
		fprintn("\n[Day 6]\n  Part 1");

		int days = 80;

		for (auto d = 0; d < days; d++) {
			int newborns = 0;

			for (auto& f : fish) {
				if (f == 0) {
					newborns++;
					f = 6;
				}
				else f -= 1;
			}

			// add in any newborns
			for (int j = 0; j < newborns; j++)
				fish.push_back(8);
		}

		fprintn("\tFish after {} days: {}", days, fish.size());
	}

	void part02(const std::span<int> inputFish) {
		fprintn("  Part 2");

		// sim 128 days for each possible age 0-8. use the resulting fish vector and counts
		// to create a 256 day lookup table for ages 0-8. apply the table to the input ages.
		std::vector<ui64> count256{ 0,0,0,0,0,0,0,0,0 };
		std::vector<std::vector<ui8>> fish128;
		int days = 128;

		for (auto age = 0; age < 9; age++) {
			std::vector<ui8> fish;
			fish.push_back(age);

			for (auto d = 0; d < days; d++) {
				ui64 newborns = 0;

				for (auto& f : fish) {
					if (f == 0) {
						newborns++;
						f = 6;
					}
					else f -= 1;
				}

				// add in any newborns
				for (int j = 0; j < newborns; j++)
					fish.push_back(8);
			}

			fish128.push_back(fish);
		}

		// using 128 days of 0-8 growth to sum and lookup the next 128 days
		// resulting in a lookup table for 256 days for all initial values
		for (auto i = 0; i < 9; i++) {
			ui64 sum = fish128[i].size();
			for (auto f : fish128[i])
				sum += fish128[f].size() - 1; // exclude initial fish

			count256[i] = sum;
		}

		// sum the actual inputs with the 0-8 age 256 day table
		ui64 answer = 0;
		for (auto f : inputFish)
			answer += count256[f];

		fprintn("\tFish after {} days: {}", days * 2, answer);
	}

	void run() {
		std::vector<int> fish;

		std::ifstream input{ "./data/day06/input.txt" };
		if (input.is_open())
		{
			std::string line;
			std::getline(input, line);

			fish = splitNumbers(line, ",");

			input.close();
		}

		part01(fish);
		part02(fish);
	}

} // namespace day06