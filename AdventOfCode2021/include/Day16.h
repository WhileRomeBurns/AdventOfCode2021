#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <string_view>
#include <vector>
#include <span>
#include <map>
#include <cassert>
#include <cstdint>
#include <stack>
#include <limits>

#include "Utilities.h"

namespace day16 {
	typedef std::int16_t i16t;
	typedef std::int32_t i32t;
	typedef std::int64_t i64t;
	typedef std::uint64_t ui64t;

	std::map<char, std::string> hexlookup{
		{'0',"0000"},{'1',"0001"},{'2',"0010"},{'3',"0011"},
		{'4',"0100"},{'5',"0101"},{'6',"0110"},{'7',"0111"},
		{'8',"1000"},{'9',"1001"},{'A',"1010"},{'B',"1011"},
		{'C',"1100"},{'D',"1101"},{'E',"1110"},{'F',"1111"}
	};

	std::string hexToBin(std::string_view data) {
		std::string bin;
		for (char c : data)
			bin += hexlookup[c];

		return bin;
	}

	ui64t binToDec(const std::string& bin) {
		return std::stoull(bin, nullptr, 2);
	}

	i16t binToDec16(const std::string& bin) {
		return i16t(std::stoi(bin, nullptr, 2));
	}

	struct Packet {
		i16t  version{ 0 };
		i16t  typeId{ 0 };
		ui64t value{ 0 };
		std::vector<Packet> subpackets;
	};

	class Parser {
	public:
		Parser(std::string_view binaryData) : bin(binaryData.data(), binaryData.size()) {}

		void parseLiteral(Packet& p) {
			std::string lit;

			bool reading = true;

			while (reading) {
				if (bin.substr(pos, 1) == "0")
					reading = false;
				pos++;

				lit += bin.substr(pos, 4);
				pos += 4;
			}

			p.value = binToDec(lit);
		}

		void parseOperator(Packet& p) {
			auto lengthTypeId = binToDec16(bin.substr(pos, 1));
			pos++;

			ui64t lengthSubPack;

			if (lengthTypeId == 0) { // specifies a number of bits
				lengthSubPack = binToDec(bin.substr(pos, 15));
				pos += 15;

				auto endPos = pos + lengthSubPack;
				while (pos < endPos)
					p.subpackets.push_back(nextPacket());
			}
			else { // specifies a number of packets
				lengthSubPack = binToDec(bin.substr(pos, 11));
				pos += 11;

				for (auto i = 0; i < lengthSubPack; i++)
					p.subpackets.push_back(nextPacket());
			}
		}

		Packet nextPacket() {
			Packet p;

			p.version = binToDec16(bin.substr(pos, 3));
			pos += 3;
			p.typeId = binToDec16(bin.substr(pos, 3));
			pos += 3;

			if (p.typeId == 4)
				parseLiteral(p);
			else
				parseOperator(p);

			return p;
		}

		void parse() {
			top = nextPacket();
		}

		ui64t sumOfVersions(const Packet& p) {
			ui64t sum = p.version;
			for (const auto& sp : p.subpackets)
				sum += sumOfVersions(sp);
			return sum;
		}

		ui64t sumOfVersions() {
			return sumOfVersions(top);
		}

		ui64t applyOperators(const Packet& p) {
			std::vector<ui64t> values;

			for (const auto& sp : p.subpackets) {
				if (sp.typeId == 4)
					values.push_back(sp.value);
				else
					values.push_back(applyOperators(sp));
			}

			ui64t r = 0;

			switch (p.typeId) {
			case 0:
				for (auto v : values)
					r += v;
				return r;
			case 1:
				r = 1;
				for (auto v : values)
					r *= v;
				return r;
			case 2:
				r = std::numeric_limits<ui64t>::max();
				for (auto v : values)
					if (v < r)
						r = v;
				return r;
			case 3:
				for (auto v : values)
					if (v > r)
						r = v;
				return r;
			case 4:
			default:
				return p.value;
			case 5:
				return ui64t(values[0] > values[1]);
			case 6:
				return ui64t(values[0] < values[1]);
			case 7:
				return ui64t(values[0] == values[1]);
			}
		}

		ui64t applyOperators() {
			return applyOperators(top);
		}

		void print(const Packet& p, int depth = 0) {
			std::string pad(depth + 1, '-');
			fprintn("{}version       :  {}", pad, p.version);
			fprintn("{}typeId        :  {}", pad, p.typeId);
			if (p.typeId == 4)
				fprintn("{}value         :  {}", pad, p.value);
			else
				fprintn("{}subpackets    :  {}", pad, p.subpackets.size());
			fprintn("{}", pad);

			for (const auto& sub : p.subpackets)
				print(sub, depth + 1);
		}

		void print() {
			print(top, 0);
		}

	private:
		std::string bin; // binary data
		i32t pos{ 0 };   // as we scan thru binary data, we'll be incrementing pos with each read
		Packet top;      // first packet in the hierarchy
	};

	void part01(Parser& parser) {
		fprint("\n[Day 16]\n  Part 1\n");
		parser.parse();
		fprintn("Sum of versions: {}", parser.sumOfVersions());
	}

	void part02(Parser& parser) {
		fprint("  Part 2\n");
		fprintn("Result of operations: {}", parser.applyOperators());
	}

	void run() {
		std::string data;

		std::ifstream input{ "./data/day16/input.txt" };
		if (input.is_open())
		{
			std::getline(input, data);
			input.close();
		}

		auto bin = hexToBin(data);
		Parser parser{ bin };

		part01(parser);
		part02(parser);
	}

} // namespace day16