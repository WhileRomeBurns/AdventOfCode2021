#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <span>
#include <numeric>
#include <cmath>
#include <format>
#include <tuple>

#include "Utilities.h"

namespace day07 {

	int computeIncreasingCost(int dist) {
		if (dist % 2 == 0) {
			return (dist / 2) * (dist + 1);
		}
		else {
			auto middle = ((dist - 1) / 2);
			return (middle * (dist + 1)) + middle + 1;
		}
	}

	std::tuple<int, int> findBestPosition(std::span<int> positions, bool increasingCost) {
		// pick a start using mean or mode
		auto sum = std::accumulate(positions.begin(), positions.end(), 0);
		auto avg = static_cast<double>(sum) / static_cast<double>(positions.size());
		auto startGuess = static_cast<int>(avg);

		auto bestFuel = 99999999;
		auto bestGuess = 0;

		for (auto dir : { -1, 1 }) {

			auto lastFuel = 99999999;

			for (auto i = 0; i < positions.size(); i++) {
				auto guess = i * dir + startGuess;

				// check the guess
				int fuel = 0;
				for (auto pos : positions) {
					auto dist = std::abs(guess - pos);

					if (increasingCost)
						fuel += computeIncreasingCost(dist);
					else
						fuel += dist;
				}

				// if we're increasing it's fruitless, let's try the other way
				if (fuel > lastFuel)
					break;
				lastFuel = fuel;

				if (fuel < bestFuel) {
					bestFuel = fuel;
					bestGuess = guess;
				}
			}
		}

		return { bestGuess, bestFuel };
	}

	void part01(std::span<int> positions) {
		fprintn("\n[Day 7]\n  Part 1");

		auto [bestGuess, bestFuel] = findBestPosition(positions, false);

		fprintn("\tBest position: {} | Fuel used: {}", bestGuess, bestFuel);
	}


	void part02(std::span<int> positions) {
		fprintn("  Part 2");

		auto [bestGuess, bestFuel] = findBestPosition(positions, true);

		fprintn("\tBest position: {} | Fuel used: {}", bestGuess, bestFuel);
	}

	void run() {
		std::vector<int> positions;

		std::ifstream input{ "./data/day07/input.txt" };
		if (input.is_open())
		{
			std::string line;
			while (std::getline(input, line))
			{
				positions = splitNumbers(line, ",");
				break;
			}
			input.close();
		}

		part01(positions);
		part02(positions);
	}

} // namespace day07