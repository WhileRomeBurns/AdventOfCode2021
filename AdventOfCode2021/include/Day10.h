#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <string_view>
#include <vector>
#include <stack>
#include <span>
#include <algorithm>

#include "Utilities.h"

namespace day10 {
	char getPair(char c) {
		switch (c) {
		case '(':
			return ')';
		case '[':
			return ']';
		case '{':
			return '}';
		case '<':
			return '>';
		case ')':
			return '(';
		case ']':
			return '[';
		case '}':
			return '{';
		case '>':
			return '<';
		default:
			return c;
		}
	}

	auto illegalScore(char c) {
		switch (c) {
		case ')':
			return 3;
		case ']':
			return 57;
		case '}':
			return 1197;
		case '>':
			return 25137;
		default:
			return 0;
		}
	}

	auto autocompleteScore(char c) {
		switch (c) {
		case ')':
			return 1;
		case ']':
			return 2;
		case '}':
			return 3;
		case '>':
			return 4;
		default:
			return 0;
		}
	}

	bool isOpen(char c) {
		return c == '(' || c == '[' || c == '{' || c == '<';
	}

	int scoreLine(std::string_view line) {
		std::stack<char> stk;

		for (char c : line) {
			if (isOpen(c))
				stk.push(c);
			else if (stk.top() == getPair(c))
				stk.pop();
			else
				return illegalScore(c);
		}

		return 0;
	}

	long long int completeLine(std::string_view line) {
		std::stack<char> stk;

		for (char c : line) {
			if (isOpen(c))
				stk.push(c);
			else if (stk.top() == getPair(c))
				stk.pop();
		}

		long long int score = 0;
		while (!stk.empty()) {
			score = (score * 5) + autocompleteScore(getPair(stk.top()));
			stk.pop();
		}

		return score;
	}

	void part01(std::span<std::string> data) {
		fprintn("\n[Day 10]\n  Part 1");

		auto score = 0;
		for (std::string_view line : data)
			score += scoreLine(line);

		fprintn("\tIllegal Score: {}", score);
	}


	void part02(std::span<std::string> data) {
		fprintn("  Part 2");

		std::vector<std::string> incompleteLines;
		std::vector<long long int> scores;

		for (const auto& line : data)
			if (scoreLine(line) == 0)
				scores.push_back(completeLine(line));

		std::sort(scores.begin(), scores.end());

		fprintn("\tMedian Autocomplete Score: {}", scores[(scores.size() - 1) / 2]);
	}

	void run() {
		std::vector<std::string> data;

		std::ifstream input{ "./data/day10/input.txt" };
		if (input.is_open())
		{
			std::string line;
			while (std::getline(input, line))
				data.push_back(line);
			input.close();
		}

		part01(data);
		part02(data);
	}

} // namespace day10