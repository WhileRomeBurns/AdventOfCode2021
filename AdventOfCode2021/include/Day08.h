#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <string_view>
#include <vector>
#include <span>
#include <cassert>
#include <algorithm>
#include <array>

#include "Utilities.h"

namespace day08 {
	typedef std::span<std::string> SpanString;
	typedef std::vector<std::string> VecString;
	typedef std::span<VecString> InputSpan;
	typedef std::vector<VecString> InputVec;

	class Grid {
	public:
		Grid(std::span<std::string> inOutClues) {
			reset();
			setClues(inOutClues);
		}

		void reset() {
			for (auto& c : grid)
				c = { 'a', 'b', 'c', 'd', 'e', 'f', 'g' };
			guesses.clear();
		}

		void setClues(SpanString inOutClues) {
			int i = 0;
			for (auto& clue : inOutClues) {
				for (char c : clue) {
					clues[i].push_back(c);
				}
				i++;
			}
		}

		void getNextPermutation(std::span<int> currentPerm) {
			for (int i = int(currentPerm.size()) - 1; i >= 0; i--) {
				if (currentPerm[i] < 6) {
					currentPerm[i]++;

					for (auto j = i + 1; j < currentPerm.size(); j++)
						currentPerm[j] = 0;

					break;
				}
			}
		}

		bool isValidPermutation(std::span<int> currentPerm) {
			std::vector<int> count = { 0, 0, 0, 0, 0, 0, 0 };

			for (auto i : currentPerm)
				count[i]++;

			for (auto c : count)
				if (c != 1)
					return false;

			return true;
		}

		bool solvesClue(std::span<int> currentPerm) {
			for (int i = 0; i < grid.size(); i++) {

				bool foundInCurrentSegment = false;

				for (char possible : grid[i]) {
					if (possible == char(currentPerm[i] + 97)) {
						foundInCurrentSegment = true;
						break;
					}
				}

				if (!foundInCurrentSegment)
					return false;
			}

			return true;
		}

		bool findChar(const std::vector<char> clue, char segment) {
			for (auto c : clue)
				if (c == segment)
					return true;

			return false;
		}

		bool maskExists(const std::array<int, 7>& digitMask) {
			for (const auto& m : masks)
				if (m == digitMask)
					return true;
			return false;
		}

		int maskToDigit(const std::array<int, 7>& digitMask) {
			int i = 0;
			for (const auto& m : masks) {
				if (m == digitMask)
					return i;
				i++;
			}
			return 0;
		}

		int decode() {
			// find correct guess by making a mask from the guess and the clue
			// if all the masks exists are valid, this is our solution
			int correctGuess = -1;

			int guessIdx = 0;
			for (const auto& guess : guesses) {

				int correctClues = 0;
				for (auto i = 0; i < clues.size(); i++) {

					// iterate the chars of the clue
					// find the index of that char in the guess
					// mark it on in the mask
					std::array<int, 7> digitMask{ 0,0,0,0,0,0,0 };
					for (int j = 0; j < clues[i].size(); j++) {
						for (int k = 0; k < 7; k++) {
							if (clues[i][j] == guess[k]) {
								digitMask[k] = 1;
								break;
							}
						}
					}

					if (maskExists(digitMask)) {
						correctClues++;
					}
				}

				if (correctClues == clues.size()) {
					correctGuess = guessIdx;
					break;
				}

				guessIdx++;
			}

			int place = 1000;
			int sum = 0;

			// convert the last 4 digits to an int
			for (auto i = clues.size() - 4; i < clues.size(); i++) {

				std::array<int, 7> digitMask{ 0,0,0,0,0,0,0 };
				for (int j = 0; j < clues[i].size(); j++) {
					for (int k = 0; k < 7; k++) {
						if (clues[i][j] == guesses[correctGuess][k]) {
							digitMask[k] = 1;
							break;
						}
					}
				}

				int digit = maskToDigit(digitMask);
				sum += digit * place;
				place /= 10;
			}

			return sum;
		}

		void tryPermutations() {
			std::vector<int>  current{ 0, 1, 2, 3, 4, 5, 6 };
			std::vector<int>      end{ 6, 5, 4, 3, 2, 1, 1 };

			for (auto k = 0; k < 100000000; k++) {
				if (current == end)
					break;

				if (isValidPermutation(current)) {

					if (solvesClue(current)) {

						std::array<char, 7> guess;
						for (auto i = 0; i < current.size(); i++)
							guess[i] = char(current[i] + 97);

						guesses.push_back(guess);
					}
				}

				getNextPermutation(current);
			}
		}

		int solve() {
			applyKnown();
			tryPermutations();
			return decode();
		}

		void mask(char segment, int numberMask) {
			// takes the current segment char and masks it from the grid possibilities
			for (auto i = 0; i < grid.size(); i++)
				if (masks[numberMask][i] == 0)
					std::erase(grid[i], segment);
		}

		void mask(std::span<char> segments, int numberMask) {
			// takes the all the segment chars in segments and mask them from the grid possibilities
			for (auto s : segments)
				mask(s, numberMask);
		}

		void applyKnown() {
			for (auto& segs : clues) {
				if (segs.size() == 2)
					mask(segs, 1);
				else if (segs.size() == 3)
					mask(segs, 7);
				else if (segs.size() == 4)
					mask(segs, 4);
				else if (segs.size() == 7)
					mask(segs, 8); // useless as all are lit
			}
		}

		void printGrid() {
			fprint("\n");
			fprintn("     {}", grid[0]);
			fprintn("{}   {}", grid[1], grid[2]);
			fprintn("     {}", grid[3]);
			fprintn("{}   {}", grid[4], grid[5]);
			fprintn("     {}", grid[6]);
		}

	private:
		std::array<std::vector<char>, 14>  clues;   // vector of 14 strings representing scrambled number segments: 'eag', 'ag', etc.
		std::array<std::vector<char>, 7>   grid;    // represents possibilites. initially 'abcdefg', etc.
		std::vector<std::array<char, 7>>   guesses;
		std::array<std::array<int, 7>, 10> masks{ { // which segment is lit
			{1,1,1,0,1,1,1}, // 0
			{0,0,1,0,0,1,0}, // 1
			{1,0,1,1,1,0,1}, // 2
			{1,0,1,1,0,1,1}, // 3
			{0,1,1,1,0,1,0}, // 4
			{1,1,0,1,0,1,1}, // 5
			{1,1,0,1,1,1,1}, // 6
			{1,0,1,0,0,1,0}, // 7
			{1,1,1,1,1,1,1}, // 8
			{1,1,1,1,0,1,1}  // 9
		} };
	};

	void print(InputSpan in, InputSpan out) {
		for (auto i = 0; i < in.size(); i++) {
			printVec(in[i], " ", false);
			std::cout << "| ";
			printVec(out[i]);
		}
	}

	void part01(InputSpan in, InputSpan out) {
		fprintn("\n[Day 8]\n  Part 1");

		auto sum = 0;
		for (auto& segments : out) {
			for (auto& s : segments) {
				if (s.size() == 2 || s.size() == 3 || s.size() == 4 || s.size() == 7)
					sum++;
			}
		}

		fprintn("\tSum of unique segments in output: {}", sum);
	}

	void part02(InputSpan in, InputSpan out) {
		fprintn("  Part 2");

		long long sum = 0;

		// combine in / out 
		for (auto i = 0; i < in.size(); i++) {
			VecString inOutClues;
			for (auto& c : in[i])
				inOutClues.push_back(c);
			for (auto& c : out[i])
				inOutClues.push_back(c);

			auto grid = Grid{ inOutClues };
			sum += grid.solve();
		}

		fprintn("\tSum outputs: {}", sum);
	}

	void run() {
		InputVec in;
		InputVec out;

		std::ifstream input{ "./data/day08/input.txt" };
		if (input.is_open())
		{
			std::string line;
			while (std::getline(input, line))
			{
				auto [a, b] = splitPairSv(line, " | ");

				in.push_back(splitStrings(a, " "));
				out.push_back(splitStrings(b, " "));
			}
			input.close();
		}

		assert(in.size() == out.size());

		part01(in, out);
		part02(in, out);
	}

} // namespace day08