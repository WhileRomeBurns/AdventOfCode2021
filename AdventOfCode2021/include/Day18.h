#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <string_view>
#include <vector>
#include <span>
#include <memory>
//#include <variant>
//#include <algorithm>
//#include <numeric>
//#include <limits>
//#include <utility>
#include <cassert>

#include "Utilities.h"

namespace day18 {
	class Snail;
	typedef std::shared_ptr<Snail> SnailPtr;
	typedef std::weak_ptr<Snail> WeakSnailPtr;

	enum class PtrSide { None, Right, Left, Both };

	class Snail {
	public:
		Snail() = default;
		Snail(int lhs, int rhs) : regLt(lhs), regRt(rhs), side(PtrSide::None) {}
		Snail(int lhs, SnailPtr rhs) : regLt(lhs), snailRt(std::move(rhs)), side(PtrSide::Right) {}
		Snail(SnailPtr lhs, int rhs) : snailLt(std::move(lhs)), regRt(rhs), side(PtrSide::Left) {}
		Snail(SnailPtr lhs, SnailPtr rhs) : snailLt(std::move(lhs)), snailRt(std::move(rhs)), side(PtrSide::Both) {}

		int          regLt{ 0 };
		int          regRt{ 0 };
		SnailPtr     snailLt;
		SnailPtr     snailRt;
		WeakSnailPtr parent;                      // holding a weak ptr to parent allows walking up the hierarchy
		//bool         exploading{ false };         // for two pass exploding
		PtrSide         side{ PtrSide::None };          // side holding a nested child snail number, if any
		PtrSide         sideRelParent{ PtrSide::None }; // when walking up the hierarchy, we're this side of our parent
	};

	bool isRegular(std::string_view sv) {
		for (const auto c : sv)
			if (c == '[' || c == ']')
				return false;
		return true;
	}

	int outerComma(std::string_view sv) {
		auto depth = 0;
		auto i = 0;

		for (; i < sv.size(); i++) {
			if (sv[i] == '[')
				depth++;
			else if (sv[i] == ']')
				depth--;
			else if (sv[i] == ',' && depth == 1)
				break;
		}

		return i;
	}

	SnailPtr parseSnail(std::string_view line) {
		// example: "[[[2,4],5],[7,[1,9]]]"
		//   find first comma one bracket deep (outerComma) and split in two strings
		//		lhs="[[[2,4],5]" && rhs="[7,[1,9]]]"
		//   if the left and right are regular numbers (no brackets) we're done otherwise recursively
		//   call parseSnail() for lhs, rhs or both til we hit a pair of regular numbers
		auto i = outerComma(line);
		assert(i != 0);

		std::string_view lhs = line.substr(1, i - 1);                           // peel off '['
		std::string_view rhs = line.substr(i + 1, (line.size() - (i + 1)) - 1); // peel off ']'

		bool regularLhs = isRegular(lhs);
		bool regularRhs = isRegular(rhs);

		if (regularLhs && regularRhs)
			// ex: lhs="1", rhs="4"
			return SnailPtr(new Snail(svToInt<int>(lhs), svToInt<int>(rhs)));
		else if (regularLhs && !regularRhs)
			// ex: lhs="1", rhs="[7,[1,9]]"
			return SnailPtr(new Snail(svToInt<int>(lhs), parseSnail(rhs)));
		else if (!regularLhs && regularRhs)
			// ex: lhs="[7,[1,9]]", rhs="4"
			return SnailPtr(new Snail(parseSnail(lhs), svToInt<int>(rhs)));
		else
			// ex: lhs="[2,4]", rhs="[7,[1,9]]"
			return SnailPtr(new Snail(parseSnail(lhs), parseSnail(rhs)));
	}

	// we have to call by sharedptr reference here to assign the weakptr, *T would not work
	void resolveParents(const SnailPtr& snail, PtrSide posAsChil = PtrSide::None) {
		switch (snail->side) {
		case PtrSide::None:
			break;
		case PtrSide::Left:
			snail->snailLt->parent = snail;
			resolveParents(snail->snailLt, PtrSide::Left);
			snail->snailLt->sideRelParent = PtrSide::Left;
			break;
		case PtrSide::Right:
			snail->snailRt->parent = snail;
			resolveParents(snail->snailRt, PtrSide::Right);
			snail->snailRt->sideRelParent = PtrSide::Right;
			break;
		case PtrSide::Both:
			snail->snailLt->parent = snail;
			snail->snailLt->sideRelParent = PtrSide::Left;
			snail->snailRt->parent = snail;
			snail->snailRt->sideRelParent = PtrSide::Right;
			resolveParents(snail->snailLt, PtrSide::Left);
			resolveParents(snail->snailRt, PtrSide::Right);
			break;
		}
	}

	void printSnail(const Snail* snail) {
		fprint("[");

		switch (snail->side) {
		case PtrSide::None:
			fprint("{},{}", snail->regLt, snail->regRt);
			break;
		case PtrSide::Left:
			printSnail(snail->snailLt.get());
			fprint(",{}", snail->regRt);
			break;
		case PtrSide::Right:
			fprint("{},", snail->regLt);
			printSnail(snail->snailRt.get());
			break;
		case PtrSide::Both:
			printSnail(snail->snailLt.get());
			fprint(",");
			printSnail(snail->snailRt.get());
			break;
		}

		fprint("]");
	}

	SnailPtr addSnail(SnailPtr lhs, SnailPtr rhs) {
		// [1,4] + [2,2] = [[1,4],[2,2]]
		return SnailPtr(new Snail(std::move(lhs), std::move(rhs)));
	}

	void addToAdjacent(const SnailPtr& snail, PtrSide startSide, int value) {
		// find first regular number to left or right outside of this snail number
		// if there is a regular number in this parent snail number, we're done
		// otherwise we walk up using the parent weakptr. 
		// if we're looking for a left regular number and we came from the right (sideRelParent==Side::Right)
		// then we descend looking for the deepest right side regular number
		// [[..[7, [[1,1], 5] ]]]
		// [[3,[2,[1,1]]],[[2,2],[3,3]]]
		// // [[3,[2,[1,1]]],[1, [[2,2],2]]]
		//
		// [1,[2,2]]
		//  [[1,[2,2]],[2,2]]
		//if (snail->side == Side::Both) {}
		auto current = snail->parent.lock();
		if (!current)
			fprintn("Error!");
		auto previous = current;
		auto sideRelParent = snail->sideRelParent;

		bool searchingUp = true;
		while (searchingUp) {
			//auto sideRelParent = snail->sideRelParent;

			if (!current) {
				searchingUp = false;
				current = previous;
				sideRelParent = current->sideRelParent;
				fprintn("Hit top ");
				break;
			}

			if (startSide == PtrSide::Left) {
				if (current->side == PtrSide::Right) {
					current->regLt += value;
					return;
				}
				else if (current->side == PtrSide::Both) {
					searchingUp = false; // we could refactor this above?
				}
			}
			else {
				if (current->side == PtrSide::Left) {
					current->regRt += value;
					return;
				}
				else if (current->side == PtrSide::Both) {
					searchingUp = false; // we could refactor this above?
				}
			}

			previous = current;
			sideRelParent = current->sideRelParent;
			current = current->parent.lock();
		}
		// [[1,1],2]
		// we hit the top or we hit a snail number containing snail numbers on both sides
		// we'll descend as far as possible on one side and that's our solution
		// it may not be possible to descend, so lets check that first
		if (startSide == PtrSide::Left && current->side == PtrSide::Left)
			return;
		if (startSide == PtrSide::Right && current->side == PtrSide::Right)
			return;
		//if (startSide == current->side)
		//	return;

		auto searchDir = (startSide == PtrSide::Left ? PtrSide::Right : PtrSide::Left);

		if (searchDir == PtrSide::Right && sideRelParent == PtrSide::Left)
			return;
		if (searchDir == PtrSide::Left && sideRelParent == PtrSide::Right)
			return;
		//if (searchDir != sideRelParent )
		//	return;

		while (true) {
			if (searchDir == PtrSide::Right) {
				if (current->side == PtrSide::Both || current->side == PtrSide::Right) {
					current = current->snailRt;
					continue;
				}
				else if (current->side == PtrSide::Left || current->side == PtrSide::None) {
					current->regRt += value;
					return;
				}
			}

			if (searchDir == PtrSide::Left) {
				if (current->side == PtrSide::Both || current->side == PtrSide::Left) {
					current = current->snailLt;
					continue;
				}
				else if (current->side == PtrSide::Right || current->side == PtrSide::None) {
					current->regLt += value;
					return;
				}
			}
		}
	}

	// we have to call by sharedptr reference here, not T*
	bool reduce(const SnailPtr& snail, int depth) {
		// if a snail number is a pair of regular numbers
		//   explode when more than 4 levels deep
		//   split when value > 10

		// check if this snail was marked exploading last pass
		/*if (depth >= 3 && snail->side != Side::None) {
			// To explode a pair, the pair's left value is added to the first regular number to the left
			// of the exploding pair (if any), and the pair's right value is added to the first regular
			// number to the right of the exploding pair (if any). Exploding pairs will always consist of
			// two regular numbers. Then, the entire exploding pair is replaced with the regular number 0.
			if ((snail->side == Side::Left && snail->snailLt->exploading) || (snail->side == Side::Both && snail->snailLt->exploading)) {
				// ex: [[5,3],2] --> [0,5]
				snail->side = Side::None;
				snail->regRt += snail->snailLt->regRt;
				snail->regLt = 0;
				snail->snailLt.reset(); // boom
				return false;
			}
			else if ((snail->side == Side::Right && snail->snailRt->exploading) || (snail->side == Side::Both && snail->snailRt->exploading)) {
				// ex: [1,[8,9]] --> [9,0]
				snail->side = Side::None;
				snail->regLt += snail->snailRt->regLt;
				snail->regRt = 0;
				snail->snailRt.reset(); // boom
				return false;
			}
		}*/

		bool reduced = true;

		switch (snail->side) {
		case PtrSide::None:
			if (depth >= 4) {
				// explode! add numbers to adjacent regular numbers (tricky!)
				addToAdjacent(snail, PtrSide::Left, snail->regLt);
				addToAdjacent(snail, PtrSide::Right, snail->regRt);

				// we replace this snail number with the regular number zero in the parent
				auto parent = snail->parent.lock();
				if (parent) {
					// child was right
					if (snail->sideRelParent == PtrSide::Right) {
						snail->regRt = 0;
						if (parent->side == PtrSide::Both)
							parent->side = PtrSide::Left;
						else
							parent->side = PtrSide::None;
						// boom
						parent->snailRt.reset();
					}
					//child was left
					else if (snail->sideRelParent == PtrSide::Left) {
						snail->regLt = 0;
						if (parent->side == PtrSide::Both)
							parent->side = PtrSide::Right;
						else
							parent->side = PtrSide::None;
						// boom
						parent->snailLt.reset();
					}
				}
				reduced = false;
			}
			else if (snail->regLt > 9) {
				// split! change type to Left
				snail->side = PtrSide::Left;
				auto a = snail->regLt / 2; // division rounds down
				auto b = snail->regLt / 2 + (snail->regLt % 2 != 0); // mod rounds up odd
				snail->regRt = 0;
				snail->snailLt = SnailPtr(new Snail(a, b));
				snail->snailLt->parent = snail;
				snail->snailLt->sideRelParent = PtrSide::Left;
				//resolveParents(snail);
				reduced = false;
			}
			else if (snail->regRt > 9) {
				// split! change type to Right

				snail->side = PtrSide::Right;
				auto a = snail->regRt / 2; // division rounds down
				auto b = snail->regRt / 2 + (snail->regRt % 2 != 0); // mod rounds up odd
				snail->regRt = 0;
				snail->snailRt = SnailPtr(new Snail(a, b));
				snail->snailRt->parent = snail;
				snail->snailRt->sideRelParent = PtrSide::Right;
				//resolveParents(snail);
				reduced = false;
			}
			break;

		case PtrSide::Left:
			reduced = reduce(snail->snailLt, depth + 1);
			break;

		case PtrSide::Right:
			reduced = reduce(snail->snailRt, depth + 1);
			break;

		case PtrSide::Both:
			reduced = reduce(snail->snailLt, depth + 1);
			if (!reduced)
				break;

			reduced = reduce(snail->snailRt, depth + 1);
			break;
			//if (!reduced)
			//	break;
			//if (a == false || b == false)
			//	reduced = false;
			//auto a = reduce(snail->snailLt, depth + 1);
			//auto b = reduce(snail->snailRt, depth + 1);
			//if (a == false || b == false)
			//	reduced = false;
			//break;
		}

		return reduced;
	}

	// we have to call by sharedptr reference here, not T*
	void reduce(const SnailPtr& snail) {
		//reduce(snail, 0);

		while (reduce(snail, 0) == false) {
			printSnail(snail.get());
			fprint("\n");
		}
	}

	/*void reduce2(const SnailPtr& topSnail) {
		const auto& snail = topSnail;
		bool reduced = false;
		auto depth = 0;

		while (!reduced) {
			reduced = true;

			// check if this snail was marked exploading last pass
			if (depth >= 3 && snail->pos != Side::None) {
				// To explode a pair, the pair's left value is added to the first regular number to the left
				// of the exploding pair (if any), and the pair's right value is added to the first regular
				// number to the right of the exploding pair (if any). Exploding pairs will always consist of
				// two regular numbers. Then, the entire exploding pair is replaced with the regular number 0.
				if ((snail->pos == Side::Left && snail->snailLt->exploading) || (snail->pos == Side::Both && snail->snailLt->exploading)) {
					// ex: [[5,3],2] --> [0,5]
					snail->pos = Side::None;
					snail->regRt += snail->snailLt->regRt;
					snail->regLt = 0;
					snail->snailLt.reset(); // boom
					reduced = false;
				}
				else if ((snail->pos == Side::Right && snail->snailRt->exploading) || (snail->pos == Side::Both && snail->snailRt->exploading)) {
					// ex: [1,[8,9]] --> [9,0]
					snail->pos = Side::None;
					snail->regLt += snail->snailRt->regLt;
					snail->regRt = 0;
					snail->snailRt.reset(); // boom
					reduced = false;
				}
			}

			bool reduced = true;

			switch (snail->pos) {
			case Side::None:
				if (depth >= 4) {
					// flag to expload first thing next pass
					snail->exploading = true;
					reduced = false;
				}
				else if (snail->regLt > 9) {
					// split, change type to Left
					snail->pos = Side::Left;
					auto a = snail->regLt / 2; // division rounds down
					auto b = snail->regLt / 2 + (snail->regLt % 2 != 0); // mod rounds up odd
					snail->snailLt = SnailPtr(new Snail(a, b));
					reduced = false;
				}
				else if (snail->regRt > 9) {
					// split, change type to Right
					snail->pos = Side::Right;
					auto a = snail->regRt / 2; // division rounds down
					auto b = snail->regRt / 2 + (snail->regRt % 2 != 0); // mod rounds up odd
					snail->snailRt = SnailPtr(new Snail(a, b));
					reduced = false;
				}
				break;

			case Side::Left:
				reduced = reduce(snail->snailLt, depth + 1);
				break;

			case Side::Right:
				reduced = reduce(snail->snailRt, depth + 1);
				break;

			case Side::Both:
				auto a = reduce(snail->snailLt, depth + 1);
				auto b = reduce(snail->snailRt, depth + 1);
				if (a == false || b == false)
					reduced = false;
				break;
			}
		}
	}*/

	void part01(std::span<std::string> data) {
		fprint("\n[Day 18]\n  Part 1\n");

		auto snail = parseSnail(data[0]);
		resolveParents(snail);
		printSnail(snail.get());
		fprint("\n");

		for (auto i = 1; i < data.size(); i++) {
			auto snailRhs = parseSnail(data[i]);
			printSnail(snailRhs.get());
			fprint("\n");

			snail = addSnail(std::move(snail), std::move(snailRhs));
			resolveParents(snail);
			//reduce(snail);
		}

		printSnail(snail.get());
		fprint("\n");

		reduce(snail);
		//printSnail(snail.get());
		//fprint("\n");
	}

	void part02(std::span<std::string> data) {
		fprint("  Part 2\n");
	}

	void run() {
		std::vector<std::string> data;

		std::ifstream input{ "./data/day18/input2.txt" };
		if (input.is_open())
		{
			std::string line;
			while (std::getline(input, line))
			{
				data.push_back(line);
			}
			input.close();
		}

		part01(data);
		part02(data);
	}

} // namespace day18