#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <array>
#include <span>
#include <stdexcept>
#include <format>

#include "Utilities.h"

namespace day04 {

	class Board {
	public:
		Board() {
			std::fill(std::begin(rows), std::end(rows), std::array<int, 5>{ {0, 0, 0, 0, 0} });
			std::fill(std::begin(marked), std::end(marked), std::array<int, 5>{ {0, 0, 0, 0, 0} });
		}

		int get(int row, int col) const {
			return rows[row][col];
		}

		void set(int row, int col, int value) {
			rows[row][col] = value;
		}

		int getMarked(int row, int col) const {
			return marked[row][col];
		}

		void setMarked(int row, int col, int value) {
			marked[row][col] = value;
		}

		int sumOfUnmarked() {
			auto sum = 0;
			for (auto r = 0; r < 5; r++) {
				for (auto c = 0; c < 5; c++) {
					if (!marked[r][c]) {
						sum += rows[r][c];
					}
				}
			}
			return sum;
		}

		void markDrawn(int drawn) {
			for (auto r = 0; r < 5; r++) {
				for (auto c = 0; c < 5; c++) {
					if (rows[r][c] == drawn) {
						marked[r][c] = true;
						return;
					}
				}
			}
		}

		void setRow(std::string_view str, int row) {
			const auto numbers = splitNumbers(str);

			if (numbers.size() != 5)
				throw std::invalid_argument("Board row must have 5 entires.");

			for (auto col = 0; col < 5; col++)
				set(row, col, numbers[col]);
		}

		bool checkBingo() const {
			// check across
			for (const auto& r : marked) {
				auto sum = 0;
				for (const auto c : r)
					sum += c;
				if (sum == 5) {
					return true;
				}
			}

			// check down
			for (int i = 0; i < 5; i++) {
				auto sum = 0;
				for (const auto& r : marked) {
					sum += r[i];
				}
				if (sum == 5) {
					return true;
				}
			}

			return false;
		}

		void print() const {
			for (const auto& r : rows)
				std::cout << std::format("    {:02} {:02} {:02} {:02} {:02}\n", r[0], r[1], r[2], r[3], r[4]);
			std::cout << std::endl;
		}

		void printMarked() const {
			for (const auto& r : marked)
				std::cout << std::format("    {:02} {:02} {:02} {:02} {:02}\n", r[0], r[1], r[2], r[3], r[4]);
			std::cout << std::endl;
		}

	private:
		std::array<std::array<int, 5>, 5> rows;
		std::array<std::array<int, 5>, 5> marked;
	};

	void part01(std::span<int> drawn, std::span<Board> boards) {
		std::cout << "\n[Day 4]\n  Part 1\n";

		for (auto n : drawn) {
			for (auto& b : boards)
				b.markDrawn(n);

			for (auto& b : boards) {
				auto bingo = b.checkBingo();

				if (bingo) {
					auto sum = b.sumOfUnmarked();

					b.print();

					std::cout << "    Sum of unmarked:   " << sum << std::endl;
					std::cout << "    Last number drawn: " << n << std::endl;
					std::cout << "    Product: " << sum * n << std::endl;

					return;
				}
			}
		}
	}

	void part02(std::span<int> drawn, std::span<Board> boards) {
		std::cout << "  Part 2\n";

		std::vector<int> placements;
		placements.resize(boards.size());
		std::fill(std::begin(placements), std::end(placements), 0);

		std::vector<int> winningDraws;
		winningDraws.resize(boards.size());
		std::fill(std::begin(winningDraws), std::end(winningDraws), 0);

		auto place = 1;
		for (auto n : drawn) {
			for (auto i = 0; i < boards.size(); i++) {
				if (placements[i] == 0) {
					boards[i].markDrawn(n);
				}
			}

			for (auto i = 0; i < boards.size(); i++) {
				if (placements[i] == 0) {
					auto bingo = boards[i].checkBingo();
					if (bingo) {
						placements[i] = place;
						winningDraws[i] = n;
						place++;
					}
				}
			}
		}

		for (auto i = 0; i < placements.size(); i++) {
			if (placements[i] != placements.size())
				continue;

			auto sum = boards[i].sumOfUnmarked();
			auto n = winningDraws[i];

			boards[i].print();

			std::cout << "    Sum of unmarked:   " << sum << std::endl;
			std::cout << "    Last number drawn: " << n << std::endl;
			std::cout << "    Product: " << sum * n << std::endl;
		}
	}

	void run() {
		std::vector<int> drawn;
		std::vector<Board> boards;

		std::ifstream input{ "./data/day04/input.txt" };
		if (input.is_open())
		{
			bool drawFound = false;
			auto currentRow = 0;
			Board board{};

			std::string line;
			while (std::getline(input, line)) {
				if (!drawFound) {
					drawn = splitNumbers(line, ",");
					drawFound = true;
					continue;
				}

				if (line.length() < 2)
					continue;

				// add to the current board
				board.setRow(line, currentRow);
				currentRow++;
				if (currentRow == 5) {
					boards.push_back(board);
					currentRow = 0;
				}
			}
			input.close();
		}

		part01(drawn, boards);
		part02(drawn, boards);
	}

} // namespace day04