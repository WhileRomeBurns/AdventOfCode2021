#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <span>

namespace day01 {

	int increases(std::span<int> data, std::size_t windowSize) {
		auto previous = -1;
		auto increased = 0;

		for (std::size_t i = 0; i < data.size() - (windowSize - 1); i++) {
			auto current = 0;
			for (std::size_t j = 0; j < windowSize; j++) {
				current += data[i + j];
			}

			if (previous == -1) {
				previous = current;
			}
			else {
				if (current > previous)
					increased++;
				previous = current;
			}
		}

		return increased;
	}

	void part01(std::span<int> measurements) {
		std::cout << "\n[Day 1]\n  Part 1\n";
		std::cout << "    Depth increases: " << increases(measurements, 1) << std::endl;
	}


	void part02(std::span<int> measurements) {
		std::cout << "  Part 2\n";
		std::cout << "    Depth increases, windowed: " << increases(measurements, 3) << std::endl;
	}

	void run() {
		std::vector<int> measurements;

		std::ifstream input{ "./data/day01/input.txt" };
		if (input.is_open())
		{
			std::string line;
			while (std::getline(input, line))
				measurements.push_back(std::stoi(line));
			input.close();
		}

		part01(measurements);
		part02(measurements);
	}

} // namespace day01