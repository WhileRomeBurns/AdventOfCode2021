#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <utility>

#include "Utilities.h"

namespace day17 {
	class v2d {
	public:
		v2d& operator+=(const v2d& rhs) {
			x += rhs.x;
			y += rhs.y;
			return *this;
		}

		int x{ 0 };
		int y{ 0 };
	};

	bool inside(v2d pos, v2d pmax, v2d pmin) {
		if (pos.x >= pmin.x && pos.x <= pmax.x)
			if (pos.y >= pmin.y && pos.y <= pmax.y)
				return true;

		return false;
	}

	bool stopearly(v2d pos, v2d vel, v2d pmax, v2d pmin) {
		if (pos.y < pmin.y)
			return true; // fallen too far

		if (vel.x > 0) {
			if (pos.x > pmax.x)
				return true; // shot past right-most edge
		}

		if (vel.x < 0) {
			if (pos.x < pmin.x)
				return true; // shot past left-most edge
		}

		return false;
	}

	std::pair<bool, int> sim(v2d vel, v2d pmax, v2d pmin) {
		v2d pos{ 0,0 };
		int maxy = pos.y;

		for (auto step = 0; step < 100000; step++) {
			pos += vel;

			if (pos.y > maxy)
				maxy = pos.y;

			if (inside(pos, pmax, pmin))
				return { true, maxy };

			if (stopearly(pos, vel, pmax, pmin))
				return { false, maxy };

			// drag
			vel.x > 0 ? vel.x -= 1 : vel.x;

			// gravity
			vel.y -= 1;
		}

		return { false, maxy };
	}

	int findMaxY(v2d pmax, v2d pmin) {
		int minVelx = -1000;
		int maxVelx = 1000;
		int minVely = 1; // only interested in positive values
		int maxVely = 1000;

		int bestMaxY = 0;
		v2d bestVel{};

		for (auto vx = minVelx; vx < maxVelx; vx++) {
			for (auto vy = minVely; vy < maxVely; vy++) {
				v2d vel{ vx, vy };
				auto [hit, maxy] = sim(vel, pmax, pmin);

				if (hit && maxy > bestMaxY) {
					bestMaxY = maxy;
					bestVel = vel;
				}
			}
		}

		return bestMaxY;
	}

	int findTotalHits(v2d pmax, v2d pmin) {
		int minVelx = -1000;
		int maxVelx = 1000;
		int minVely = -1000;
		int maxVely = 1000;

		int hits = 0;

		for (auto vx = minVelx; vx < maxVelx; vx++) {
			for (auto vy = minVely; vy < maxVely; vy++) {
				v2d vel{ vx, vy };
				auto [hit, maxy] = sim(vel, pmax, pmin);

				if (hit)
					hits++;
			}
		}

		return hits;
	}

	void part01(v2d pmax, v2d pmin) {
		fprint("\n[Day 17]\n  Part 1\n");
		fprintn("Max Y Position: {}", findMaxY(pmax, pmin));
	}

	void part02(v2d pmax, v2d pmin) {
		fprint("  Part 2\n");
		fprintn("Total Hits: {}", findTotalHits(pmax, pmin));
	}

	void run() {
		std::string data;
	
		std::ifstream input{ "./data/day17/input.txt" };
		if (input.is_open())
		{
			std::getline(input, data);
			input.close();
		}

		auto x = data.find("x=");
		auto c = data.find(",");
		auto y = data.find("y=");

		if (x == data.npos || y == data.npos || c == data.npos) {
			fprintn("Parse error: {}", data);
			return;
		}

		auto [pt1x, pt2x] = splitPairInt(data.substr(x + 2, c - (x + 2)), "..");
		auto [pt1y, pt2y] = splitPairInt(data.substr(y + 2), "..");

		v2d pmax{ pt2x, pt2y };
		v2d pmin{ pt1x, pt1y };

		part01(pmax, pmin);
		part02(pmax, pmin);
	}

} // namespace day17