#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <string_view>
#include <vector>
#include <span>
#include <format>
#include <ranges>
#include <algorithm>

#include "Utilities.h"

namespace day05 {
	namespace rs = std::ranges;

	struct Line {
		Line() : x1(0), y1(0), x2(0), y2(0) {}

		Line(int X1, int Y1, int X2, int Y2) : x1(X1), y1(Y1), x2(X2), y2(Y2) {}

		void print() const { fprint("{},{} -> {},{}\n", x1, y1, x2, y2); }

		int x1, y1, x2, y2;
	};

	class Grid {
	public:
		Grid(std::size_t dim) : width(dim) {
			data.resize(dim * dim);
			std::fill(std::begin(data), std::end(data), 0);
		}

		void set(std::size_t x, std::size_t y, int value) {
			data[y * width + x] = value;
		}

		int get(std::size_t x, std::size_t y) const {
			return data[y * width + x];
		}

		void print() const {
			std::ostringstream g;
			for (auto y = 0; y < width; y++) {
				for (auto x = 0; x < width; x++) {
					g << get(x, y);
				}
				g << "\n";
			}
			std::cout << g.str() << std::endl;
		}

		int overlap() {
			auto v = 0;

			for (auto y = 0; y < width; y++)
				for (auto x = 0; x < width; x++)
					if (get(x, y) > 1) v++;

			return v;
		}

		void fillAxis(const Line& line) {
			int y1{}, y2{}, x1{}, x2{};

			// vertical, sort y
			if (line.x1 == line.x2) {
				x1 = x2 = line.x1;
				if (line.y1 <= line.y2) {
					y1 = line.y1;
					y2 = line.y2;
				}
				else {
					y1 = line.y2;
					y2 = line.y1;
				}
			}
			// horizontal, sort x
			if (line.y1 == line.y2) {
				y1 = y2 = line.y1;
				if (line.x1 <= line.x2) {
					x1 = line.x1;
					x2 = line.x2;
				}
				else {
					x1 = line.x2;
					x2 = line.x1;
				}
			}

			for (auto y = y1; y <= y2; y++) {
				for (auto x = x1; x <= x2; x++) {
					int z = get(x, y) + 1;
					set(x, y, z);
				}
			}
		}

		void fill(const Line& line) {
			// axis lines or 45 degree lines will always have length equal to the large member
			int xn = std::abs(line.x1 - line.x2) + 1;
			int yn = std::abs(line.y1 - line.y2) + 1;
			int dist = std::max(xn, yn);

			int xstart = line.x1;
			int ystart = line.y1;

			int xstep = 0;
			if (line.x1 != line.x2)
				xstep = line.x1 < line.x2 ? 1 : -1;

			int ystep = 0;
			if (line.y1 != line.y2)
				ystep = line.y1 < line.y2 ? 1 : -1;

			std::vector<int> xrange, yrange;
			xrange.resize(dist);
			yrange.resize(dist);

			rs::generate_n(xrange.begin(), dist, [xstep, &xstart]() { int r = xstart; xstart += xstep; return r; });
			rs::generate_n(yrange.begin(), dist, [ystep, &ystart]() { int r = ystart; ystart += ystep; return r; });

			for (auto n = 0; n < xrange.size(); n++) {
				auto x = xrange[n];
				auto y = yrange[n];
				int z = get(x, y) + 1;
				set(x, y, z);
			}
		}

	private:
		std::size_t width;
		std::vector<int> data;
	};

	void part01(std::span<Line> lines, size_t gridWidth) {
		std::cout << "\n[Day 5]\n  Part 1\n";

		Grid grid{ gridWidth };

		for (const auto& line : lines) {
			if (gridWidth < 20)
				line.print();
			grid.fillAxis(line);
		}

		if (gridWidth < 20)
			grid.print();
		std::cout << "\tOverlap: " << grid.overlap() << std::endl;
	}

	void part02(std::span<Line> lines, size_t gridWidth) {
		std::cout << "  Part 2\n";

		Grid grid{ gridWidth };

		for (const auto& line : lines) {
			if (gridWidth < 20)
				line.print();
			grid.fill(line);
		}

		if (gridWidth < 20)
			grid.print();
		std::cout << "\tOverlap: " << grid.overlap() << std::endl;
	}

	void run() {
		std::vector<Line> axisLines;
		std::vector<Line> allLines;
		int maxValue = 0; // to know what grid size we'll need

		std::ifstream input{ "./data/day05/input.txt" };
		if (input.is_open())
		{
			std::string line;
			while (std::getline(input, line))
			{
				const auto [a, b] = splitPairSv(line, " -> ");
				const auto [a1, a2] = splitPairSv(a, ",");
				const auto [b1, b2] = splitPairSv(b, ",");

				auto pt1x = std::stoi(a1.data());
				auto pt1y = std::stoi(a2.data());
				auto pt2x = std::stoi(b1.data());
				auto pt2y = std::stoi(b2.data());

				maxValue = std::max({ maxValue, pt1x, pt1y, pt2x, pt2y });

				// axis aligned, no diagonals 
				if (pt1x == pt2x || pt1y == pt2y)
					axisLines.emplace_back(pt1x, pt1y, pt2x, pt2y);

				allLines.emplace_back(pt1x, pt1y, pt2x, pt2y);
			}
			input.close();
		}

		maxValue += 1;

		part01(axisLines, maxValue);
		part02(allLines, maxValue);
	}

} // namespace day05