#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <string_view>
#include <vector>
#include <span>
#include <memory>
#include <variant>
//#include <algorithm>
//#include <numeric>
//#include <limits>
#include <utility>
#include <cassert>

#include "Utilities.h"

namespace day18 {

	class Snail;
	typedef std::shared_ptr<Snail> SnailPtr;
	typedef std::weak_ptr<Snail> WeakSnailPtr;

	enum class PtrSide { None, Right, Left, Both };
	enum class Side { Right, Left };

	class Snail {
	public:
		Snail() = default;
		Snail(int lhs, int rhs, int _depth) : regLt(lhs), regRt(rhs), depth(_depth), side(PtrSide::None) {}
		Snail(int lhs, SnailPtr rhs, int _depth) : regLt(lhs), snailRt(std::move(rhs)), depth(_depth), side(PtrSide::Right) {}
		Snail(SnailPtr lhs, int rhs, int _depth) : snailLt(std::move(lhs)), regRt(rhs), depth(_depth), side(PtrSide::Left) {}
		Snail(SnailPtr lhs, SnailPtr rhs, int _depth) : snailLt(std::move(lhs)), snailRt(std::move(rhs)), depth(_depth), side(PtrSide::Both) {}

		void increaseDepth() {
			depth++;
			switch (side) {
			case PtrSide::None:
				break;
			case PtrSide::Left:
				snailLt->increaseDepth();
				break;
			case PtrSide::Right:
				snailRt->increaseDepth();
				break;
			case PtrSide::Both:
				snailLt->increaseDepth();
				snailRt->increaseDepth();
				break;
			}
		}

		int          regLt{ 0 };
		int          regRt{ 0 };
		int          flatIdx{ 0 };
		int          depth{ 0 };
		SnailPtr     snailLt;
		SnailPtr     snailRt;
		WeakSnailPtr parent;                         // holding a weak ptr to parent allows walking up the hierarchy
		//bool         exploading{ false };          // for two pass exploding
		PtrSide      side{ PtrSide::None };          // side holding a nested child snail number, if any
		PtrSide      sideRelParent{ PtrSide::None }; // when walking up the hierarchy, we're this side of our parent
	};

	bool isRegular(std::string_view sv) {
		for (const auto c : sv)
			if (c == '[' || c == ']')
				return false;
		return true;
	}

	int outerComma(std::string_view sv) {
		auto depth = 0;
		auto i = 0;

		for (; i < sv.size(); i++) {
			if (sv[i] == '[')
				depth++;
			else if (sv[i] == ']')
				depth--;
			else if (sv[i] == ',' && depth == 1)
				break;
		}

		return i;
	}

	SnailPtr parseSnail(std::string_view line, int depth = 0) {
		// example: "[[[2,4],5],[7,[1,9]]]"
		//   find first comma one bracket deep (outerComma) and split in two strings
		//		lhs="[[[2,4],5]" && rhs="[7,[1,9]]]"
		//   if the left and right are regular numbers (no brackets) we're done otherwise recursively
		//   call parseSnail() for lhs, rhs or both til we hit a pair of regular numbers
		auto i = outerComma(line);
		assert(i != 0);

		std::string_view lhs = line.substr(1, i - 1);                           // peel off '['
		std::string_view rhs = line.substr(i + 1, (line.size() - (i + 1)) - 1); // peel off ']'

		bool regularLhs = isRegular(lhs);
		bool regularRhs = isRegular(rhs);

		if (regularLhs && regularRhs)
			// ex: lhs="1", rhs="4"
			return SnailPtr(new Snail(svToInt<int>(lhs), svToInt<int>(rhs), depth));
		else if (regularLhs && !regularRhs)
			// ex: lhs="1", rhs="[7,[1,9]]"
			return SnailPtr(new Snail(svToInt<int>(lhs), parseSnail(rhs, depth + 1), depth));
		else if (!regularLhs && regularRhs)
			// ex: lhs="[7,[1,9]]", rhs="4"
			return SnailPtr(new Snail(parseSnail(lhs, depth + 1), svToInt<int>(rhs), depth));
		else
			// ex: lhs="[2,4]", rhs="[7,[1,9]]"
			return SnailPtr(new Snail(parseSnail(lhs, depth + 1), parseSnail(rhs, depth + 1), depth));
	}

	// we have to call by sharedptr reference here to assign the weakptr, *T would not work
	void resolveParents(const SnailPtr& snail, PtrSide posAsChil = PtrSide::None) {
		switch (snail->side) {
		case PtrSide::None:
			break;
		case PtrSide::Left:
			snail->snailLt->parent = snail;
			resolveParents(snail->snailLt, PtrSide::Left);
			snail->snailLt->sideRelParent = PtrSide::Left;
			break;
		case PtrSide::Right:
			snail->snailRt->parent = snail;
			resolveParents(snail->snailRt, PtrSide::Right);
			snail->snailRt->sideRelParent = PtrSide::Right;
			break;
		case PtrSide::Both:
			snail->snailLt->parent = snail;
			snail->snailLt->sideRelParent = PtrSide::Left;
			snail->snailRt->parent = snail;
			snail->snailRt->sideRelParent = PtrSide::Right;
			resolveParents(snail->snailLt, PtrSide::Left);
			resolveParents(snail->snailRt, PtrSide::Right);
			break;
		}
	}

	SnailPtr descend(const SnailPtr& snail, bool left = true) {
		// walk down til we can't
		// returns the current snailptr if we cannot descend
		auto current = snail;
		//auto depth = 0;
		while (left) {
			if (current->side == PtrSide::Left || current->side == PtrSide::Both) {
				current = current->snailLt;
				//depth++;
			}
			else if (current->side == PtrSide::Right || current->side == PtrSide::None) {
				return current;
			}
		}
		while (!left) {
			if (current->side == PtrSide::Right || current->side == PtrSide::Both) {
				current = current->snailRt;
				//depth++;
			}
			else if (current->side == PtrSide::Left || current->side == PtrSide::None) {
				return current;
			}
		}
		return current;
	}

	SnailPtr ascend(const SnailPtr& snail) {
		// walk up till we can't
		SnailPtr prev = snail;
		auto current = snail->parent.lock();

		while (current) {
			prev = current;
			current = current->parent.lock();
		}
		return prev;
	}

	//std::vector<WeakSnailPtr> buildFlatmap(const SnailPtr& snail) {
	//	bool searchingDown = true;
	//	std::vector<WeakSnailPtr> flatmap;


	//}

	std::string snailToString(const Snail* snail) {
		std::string r = "[";

		switch (snail->side) {
		case PtrSide::None:
			r.append(std::format("{},{}", snail->regLt, snail->regRt));
			break;
		case PtrSide::Left:
			r.append(snailToString(snail->snailLt.get()));
			r.append(std::format(",{}", snail->regRt));
			break;
		case PtrSide::Right:
			r.append(std::format("{},", snail->regLt));
			r.append(snailToString(snail->snailRt.get()));
			break;
		case PtrSide::Both:
			r.append(snailToString(snail->snailLt.get()));
			r.append(",");
			r.append(snailToString(snail->snailRt.get()));
			break;
		}

		r.append("]");
		return r;
	}

	void printSnail(const Snail* snail) {
		fprint(snailToString(snail));
	}

	void buildFlatList(SnailPtr snail, std::vector<std::pair<WeakSnailPtr, Side>>& flat) {
		switch (snail->side) {
		case PtrSide::None:
			flat.emplace_back(snail, Side::Left);
			flat.emplace_back(snail, Side::Right);
			break;
		case PtrSide::Left:
			buildFlatList(snail->snailLt, flat);
			flat.emplace_back(snail, Side::Right);
			break;
		case PtrSide::Right:
			flat.emplace_back(snail, Side::Left);
			buildFlatList(snail->snailRt, flat);
			break;
		case PtrSide::Both:
			buildFlatList(snail->snailLt, flat);
			buildFlatList(snail->snailRt, flat);
			break;
		}
	}

	void updateSnailFlatIndex(std::vector<std::pair<WeakSnailPtr, Side>>& flat) {
		auto i = 0;
		for (auto& p : flat) {
			if (auto snail = p.first.lock())
				snail->flatIdx = i;
			i++;
		}
	}

	std::pair<std::string, std::string> flatListToString(std::vector<std::pair<WeakSnailPtr, Side>>& flat) {
		std::string order;
		std::string value;

		auto i = 0;
		for (auto& p : flat) {
			if (i != 0) {
				value.append(" ");
				order.append(" ");
			}

			if (auto snail = p.first.lock()) {
				if (p.second == Side::Left)
					value.append(std::format("{}", snail->regLt));
				else
					value.append(std::format("{}", snail->regRt));
			}

			order.append(std::format("{}", i));
			i++;
		}

		return std::make_pair(order, value);
	}

	void printFlatList(std::pair<std::string, std::string> flatList) {
		fprintn(flatList.first);
		fprintn(flatList.second);
	}

	void addToAdjacent(const SnailPtr& snail, PtrSide startSide, int value) {
		// find first regular number to left or right outside of this snail number
		// if there is a regular number in this parent snail number, we're done
		// otherwise we walk up using the weakptr to the next parent.
		// if we're looking for a left regular number and we came from the right (sideRelParent==PtrSide::Right)
		// then we descend looking for the deepest right side regular number
		// [[..[7, [[1,1], 5] ]]]
		// [[3,[2,[1,1]]],[[2,2],[3,3]]]
		// // [[3,[2,[1,1]]],[1, [[2,2],2]]]
		//
		// [1,[2,2]]
		//  [[1,[2,2]],[2,2]]
		//if (snail->side == Side::Both) {}
		auto current = snail->parent.lock();
		auto previous = snail->parent.lock();
		if (!current || !previous)
			fprintn("Error! Can't get parent lock for snail number while adding.");
		auto sideRelParent = snail->sideRelParent;

		bool searchingUp = true;
		while (searchingUp) {
			//auto sideRelParent = snail->sideRelParent;

			if (!current) {
				searchingUp = false;
				current = previous;
				sideRelParent = current->sideRelParent;
				fprintn("Hit top ");
				break;
			}

			if (startSide == PtrSide::Left) {
				if (current->side == PtrSide::Right) {
					current->regLt += value;
					return;
				}
				else if (current->side == PtrSide::Both) {
					searchingUp = false; // we could refactor this above?
				}
			}
			else {
				if (current->side == PtrSide::Left) {
					current->regRt += value;
					return;
				}
				else if (current->side == PtrSide::Both) {
					searchingUp = false; // we could refactor this above?
				}
			}

			previous = current;
			sideRelParent = current->sideRelParent;
			current = current->parent.lock();
		}
		// [[1,1],2]
		// we hit the top or we hit a snail number containing snail numbers on both sides
		// we'll descend as far as possible on one side and that's our solution
		// it may not be possible to descend, so lets check that first
		if (startSide == PtrSide::Left && current->side == PtrSide::Left)
			return;
		if (startSide == PtrSide::Right && current->side == PtrSide::Right)
			return;
		//if (startSide == current->side)
		//	return;

		auto searchDir = (startSide == PtrSide::Left ? PtrSide::Right : PtrSide::Left);

		if (searchDir == PtrSide::Right && sideRelParent == PtrSide::Left)
			return;
		if (searchDir == PtrSide::Left && sideRelParent == PtrSide::Right)
			return;
		//if (searchDir != sideRelParent )
		//	return;

		while (true) {
			if (searchDir == PtrSide::Right) {
				if (current->side == PtrSide::Both || current->side == PtrSide::Right) {
					current = current->snailRt;
					continue;
				}
				else if (current->side == PtrSide::Left || current->side == PtrSide::None) {
					current->regRt += value;
					return;
				}
			}

			if (searchDir == PtrSide::Left) {
				if (current->side == PtrSide::Both || current->side == PtrSide::Left) {
					current = current->snailLt;
					continue;
				}
				else if (current->side == PtrSide::Right || current->side == PtrSide::None) {
					current->regLt += value;
					return;
				}
			}
		}
	}

	SnailPtr addSnail(SnailPtr lhs, SnailPtr rhs) {
		// [1,4] + [2,2] = [[1,4],[2,2]]
		// lhs and rhs snails will be nested one level deeper, so we must increase their depth
		// increaseDepth() will also increase the depth of all child snail numbers
		lhs->increaseDepth();
		rhs->increaseDepth();
		return SnailPtr(new Snail(std::move(lhs), std::move(rhs), 0));
	}

	void split(std::vector<std::pair<WeakSnailPtr, Side>>& flat, int idx) {
		// To split a regular number, replace it with a pair; the left element of the pair should be
		// the regular number divided by two and rounded down, while the right element of the pair
		// should be the regular number divided by two and rounded up. For example, 10 becomes [5,5],
		// 11 becomes [5,6], 12 becomes [6,6], and so on.
	}

	void explode(std::vector<std::pair<WeakSnailPtr, Side>>& flat, int idx) {
		// To explode a pair, the pair's left value is added to the first regular number to the left of
		// the exploding pair (if any), and the pair's right value is added to the first regular number
		// to the right of the exploding pair (if any). Exploding pairs will always consist of two
		// regular numbers. Then, the entire exploding pair is replaced with the regular number 0.
	}

	void reduce(std::vector<std::pair<WeakSnailPtr, Side>>& flat) {
		// 1) If any pair is nested inside four pairs, the leftmost such pair explodes.
		// 2) If any regular number is 10 or greater, the leftmost such regular number splits.

	}
}  // namespace day18