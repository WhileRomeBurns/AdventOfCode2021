#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <span>
#include <utility>
#include <set>
#include <map>
#include <numeric>

#include "Utilities.h"

namespace day14 {

	std::set<char> findElements(std::string_view chain, std::span<std::pair<std::string, std::string>> rules) {
		std::set<char> elems;

		for (const auto& [a, b] : rules) {
			elems.insert(a[0]);
			elems.insert(a[1]);
			elems.insert(b[0]);
		}

		for (char c : chain)
			elems.insert(c);

		return elems;
	}

	void findInsertions(std::string_view chain, std::span<std::pair<std::string, std::string>> rules, std::vector<std::pair<std::string, size_t>>& insertions) {
		for (std::size_t i = 0; i < chain.size() - 1; i++) {
			for (const auto& [a, b] : rules)
				if (a == chain.substr(i, 2))
					insertions.emplace_back(b, i + 1);
		}
	}

	void applyInsertions(std::string& chain, std::span<std::pair<std::string, size_t>> insertions) {
		size_t offset = 0;
		for (const auto& [a, b] : insertions)
			chain.insert(b + offset++, a);
	}

	size_t solution01(std::string_view chain, const std::set<char>& elements) {
		std::map<char, size_t> counts;

		for (auto e : elements)
			counts[e] = 0;
		for (char c : chain)
			counts[c]++;

		size_t minCount = std::numeric_limits<size_t>::max();
		size_t maxCount = 0;

		for (auto [elem, count] : counts) {
			if (count > maxCount)
				maxCount = count;

			if (count < minCount)
				minCount = count;
		}

		return maxCount - minCount;
	}

	size_t maxMinDifferenceOfAtoms(const std::map<char, size_t>& atoms) {
		size_t minCount = std::numeric_limits<size_t>::max();
		size_t maxCount = 0;

		for (auto [elem, count] : atoms) {
			if (count > maxCount)
				maxCount = count;

			if (count < minCount)
				minCount = count;
		}

		return maxCount - minCount;
	}

	void part01(std::string chain, std::vector<std::pair<std::string, std::string>> rules) {
		fprintn("\n[Day 14]\n  Part 1");

		for (auto i = 0; i < 10; i++) {
			std::vector<std::pair<std::string, size_t>> insertions;
			findInsertions(chain, rules, insertions);
			applyInsertions(chain, insertions);
		}

		fprintn("\tMost common element minus least common: {}", solution01(chain, findElements(chain, rules)));
	}

	void part02(std::string chain, std::vector<std::pair<std::string, std::string>> rules) {
		fprintn("  Part 2\n");

		// C, N, H, etc
		std::map<char, size_t> countAtoms;
		for (auto c : findElements(chain, rules))
			countAtoms[c] = 0;

		for (std::size_t i = 0; i < chain.size(); i++)
			countAtoms[chain[i]]++;

		// NN, CN, CH, etc
		std::map<std::string, size_t> countPairs;
		for (auto& r : rules)
			countPairs[r.first] = 0;

		for (std::size_t i = 0; i < chain.size() - 1; i++)
			countPairs[chain.substr(i, 2)]++;

		// map of the pairs produced by this pair. example: NN -> C means we get NCN
		// which is computed as NC and CN for the next generation. the map stores:
		// rulePair[NN] = {"NC", "CN"}
		std::map<std::string, std::pair<std::string, std::string>> rulePair;
		for (auto& r : rules)
			rulePair[r.first] = { r.first[0] + r.second, r.second + r.first[1] };

		for (auto i = 0; i < 40; i++) {
			const auto countOld = countPairs; // copy

			for (const auto& [currentPair, currentCount] : countOld) {
				const auto& [newLeft, newRight] = rulePair[currentPair];

				countPairs[currentPair] -= currentCount;
				countPairs[newLeft] += currentCount;
				countPairs[newRight] += currentCount;

				countAtoms[newLeft[1]] += currentCount;
			}
		}

		fprintn("\tMost common element minus least common: {}", maxMinDifferenceOfAtoms(countAtoms));
	}

	void run() {
		std::string chain;
		std::vector<std::pair<std::string, std::string>> rules;

		std::ifstream input{ "./data/day14/input.txt" };
		if (input.is_open())
		{
			std::string line;
			std::getline(input, line);
			chain = line;
			std::getline(input, line);

			while (std::getline(input, line))
				rules.push_back(splitPair(line, " -> "));
			input.close();
		}

		part01(chain, rules);
		part02(chain, rules);
	}

} // namespace day14