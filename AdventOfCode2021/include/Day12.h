#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <span>
#include <set>
#include <map>
#include <utility>

#include "Utilities.h"

namespace day12 {

	class Graph {
	public:
		Graph(std::span<std::pair<std::string, std::string>> edges) {
			for (const auto& edge : edges) {
				mNodes.insert(edge.first);
				mNodes.insert(edge.second);
			}

			for (auto& nd : mNodes) {
				std::vector<std::string> conn;

				for (auto& [a, b] : edges) {
					if (nd == a)
						conn.push_back(b);
					else if (nd == b)
						conn.push_back(a);
				}

				mConnections[nd] = conn;
				mVisits[nd] = 0;
			}

			mVisits["start"] = 1;
		}

		bool isExplored(std::string_view nextNode, std::size_t depth, std::span<std::vector<std::string>> avoid) {
			if (avoid.size() < depth + 1)
				return false;

			for (const std::string_view av : avoid[depth])
				if (nextNode == av)
					return true;

			return false;
		}

		void exploreAllRoutes() {
			std::vector<std::string> route{ "start" };
			std::vector<std::vector<std::string>> avoid{ { "start" } }; // cache of banches we've explored
			std::map<std::string, int> visits{ mVisits }; // copy of map with all nodes as keys, start = 1
			std::size_t depth = 1;

			while (depth > 0) { // exists when we eventually pop "start"

				const auto& currentNode = route[depth - 1];
				std::string nextNode{};

				for (const auto& n : mConnections.at(currentNode)) {

					if (islower(n[0]) && visits[n] > 0)
						continue;

					if (isExplored(n, depth, avoid))
						continue;

					nextNode = n;
					break;
				}

				// three posibilities:
				//    1) found no next node, possible dead end   -- pop currentNode
				//    2) found the end node                      -- count++
				//    3) found a valid node to extend the route, -- push nextNode
				if (nextNode.empty()) {
					visits[currentNode]--;
					route.pop_back();

					if (avoid.size() > depth)
						avoid.pop_back();
					depth--;
				}
				else if (nextNode == "end") {
					mSuccessfullRoutes++;

					if (avoid.size() < depth + 1)
						avoid.push_back({ nextNode });
					else
						avoid[depth].push_back(nextNode);
				}
				else {
					visits[nextNode]++;
					route.push_back(nextNode);

					if (avoid.size() < depth + 1)
						avoid.push_back({ nextNode });
					else
						avoid[depth].push_back(nextNode);
					depth++;
				}
			} // while()
		}

		void exploreAllRoutes2() {
			// in this variation, 1 small caves can be visited twice
			std::vector<std::string> route{ "start" };
			std::vector<std::vector<std::string>> avoid{ { "start" } }; // cache of banches we've explored
			std::map<std::string, int> visits{ mVisits }; // copy of map with all nodes as keys, start = 1
			std::size_t depth = 1;
			bool smallVistedTwice = false;

			while (depth > 0) { // exists when we eventually pop "start"

				const auto& currentNode = route[route.size() - 1];
				std::string nextNode{};

				for (const auto& n : mConnections.at(currentNode)) {
					if (n == "start")
						continue;

					if (islower(n[0]) && n != "end") {
						if (smallVistedTwice && visits[n] > 0)
							continue;
						if (!smallVistedTwice && visits[n] > 1)
							continue;
					}

					if (isExplored(n, depth, avoid))
						continue;

					nextNode = n;
					break;
				}

				// three posibilities:
				//    1) found no next node, possible dead end   -- pop currentNode
				//    2) found the end node                      -- count++
				//    3) found a valid node to extend the route, -- push nextNode
				if (nextNode.empty()) {

					if (islower(currentNode[0]) && visits[currentNode] == 2)
						smallVistedTwice = false;

					visits[currentNode]--;
					route.pop_back();

					if (avoid.size() > depth)
						avoid.pop_back();
					depth--;
				}
				else if (nextNode == "end") {
					mSuccessfullRoutes++;

					if (avoid.size() < depth + 1)
						avoid.push_back({ nextNode });
					else
						avoid[depth].push_back(nextNode);
				}
				else {
					if (islower(nextNode[0]) && visits[nextNode] == 1)
						smallVistedTwice = true;

					visits[nextNode]++;
					route.push_back(nextNode);

					if (avoid.size() < depth + 1)
						avoid.push_back({ nextNode });
					else
						avoid[depth].push_back(nextNode);
					depth++;
				}
			} // while()
		}

		std::size_t getSuccessfullRouteCount() {
			return mSuccessfullRoutes;
		}

		void printConnections() const {
			for (auto& [a, b] : mConnections)
				fprint("node: {:>8} ---> {}\n", a, b);
		}

	private:
		std::set<std::string> mNodes;                                 // node list
		std::map<std::string, int> mVisits;                           // used to initialize local visit map
		std::vector<std::vector<std::string>> mExploredRoutes;        // routes including dead-ends and ends
		std::map<std::string, std::vector<std::string>> mConnections; // each node's list of connections
		std::size_t mSuccessfullRoutes{ 0 };
	};

	void part01(Graph graph) {
		fprintn("\n[Day 12]\n  Part 1");

		graph.exploreAllRoutes();

		fprintn("\tNumber of routes: {}", graph.getSuccessfullRouteCount());
	}

	void part02(Graph graph) {
		fprintn("  Part 2");

		graph.exploreAllRoutes2();

		fprintn("\tNumber of routes: {}", graph.getSuccessfullRouteCount());
	}

	void run() {
		std::vector<std::pair<std::string, std::string>> edges;

		std::ifstream input{ "./data/day12/input.txt" };
		if (input.is_open())
		{
			std::string line;
			while (std::getline(input, line))
				edges.push_back(splitPair(line, "-"));
			input.close();
		}

		part01(Graph{ edges });
		part02(Graph{ edges });
	}

} // namespace day12