#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <span>
//#include <algorithm>
//#include <numeric>
//#include <limits>
//#include <utility>

#include "Utilities.h"

namespace day00 {
	void part01(std::span<std::string> data) {
		fprint("\n[Day 00]\n  Part 1\n");
	}

	void part02(std::span<std::string> data) {
		fprint("  Part 2\n");
	}

	void run() {
		std::vector<std::string> data;

		std::ifstream input{ "./data/day00/input2.txt" };
		if (input.is_open())
		{
			std::string line;
			while (std::getline(input, line))
			{
				data.push_back(line);
			}
			input.close();
		}

		part01(data);
		part02(data);
	}

} // namespace day00