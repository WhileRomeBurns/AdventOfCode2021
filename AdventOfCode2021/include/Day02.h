#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <string_view>
#include <vector>
#include <span>

#include "Utilities.h"

namespace day02 {

	enum class direction { forward, down, up };

	void part01(std::span<std::tuple<direction, int>> data) {
		auto horizontal = 0;
		auto depth = 0;

		for (const auto& [a,b] : data) {

			switch (a)
			{
			case direction::forward:
				horizontal += b;
				break;
			case direction::up:
				depth -= b;
				break;
			case direction::down:
				depth += b;
				break;
			}
		}

		std::cout << "\n[Day 2]\n  Part 1\n";
		std::cout << "    Horizontal: " << horizontal << " | Depth: " << depth << std::endl;
		std::cout << "    Product: " << horizontal*depth << std::endl;
	}


	void part02(std::span<std::tuple<direction, int>> data) {
		auto horizontal = 0;
		auto depth = 0;
		auto aim = 0;

		for (const auto& [a,b] : data) {

			switch (a)
			{
			case direction::forward:
				horizontal += b;
				depth += aim * b;
				break;
			case direction::up:
				aim -= b;
				break;
			case direction::down:
				aim += b;
				break;
			}
		}

		std::cout << "  Part 2\n";
		std::cout << "    Horizontal: " << horizontal << " | Depth: " << depth << std::endl;
		std::cout << "    Product: " << horizontal*depth << std::endl;
	}

	void run() {
		std::vector<std::tuple<direction, int>> data;

		std::ifstream input("./data/day02/input.txt");
		if (input.is_open())
		{
			std::string line;
			while (std::getline(input, line))
			{
				const auto [a, b] = splitPairSv(line, " ");

				direction dir;
				if (a == std::string_view{ "forward" }) {
					dir = direction::forward;
				}
				else if (a == std::string_view{ "up" }) {
					dir = direction::up;
				}
				else {
					dir = direction::down;
				}

				data.emplace_back(dir, std::stoi(std::string(b.begin(), b.end())));
			}
			input.close();
		}

		part01(data);
		part02(data);
	}

} // namespace day02